package com.jdbcConnect;

import java.sql.Connection;
import org.junit.Test;
import java.sql.SQLException;

import java.sql.*;



public class JdbcDataBaseConnector {

    @Test
    public  void testDB() throws ClassNotFoundException, SQLException {
        //YouTube Link = https://www.youtube.com/watch?v=CjNqKlTluxc&feature=youtu.be
        //YouTube Link2 = https://www.youtube.com/watch?v=HDRoiK0BWYs

        Class.forName("com.mysql.jdbc.Driver");
        System.out.println("Driver loaded");
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/Employeeportal","root",
                "root");
        System.out.println("connected to mysql DB");
        Statement smt = con.createStatement();
        ResultSet rs = smt.executeQuery("select * from Employeeinfo");
        while(rs.next()) {
            String Firstname = rs.getString("name");
            System.out.println("Database record is " + Firstname);

            String UserDept = rs.getString("dept");
            System.out.println("Database record is " + UserDept);

            String UserID = rs.getString("id");
            System.out.println("Database record is " + UserID);

            String UserAge = rs.getString("age");
            System.out.println("Database record is " + UserAge);

            System.out.println("Database record is " + Firstname
                    + "  " + UserDept
                    + "  " + UserID
                    + "  " + UserAge);
        }
    }

}
